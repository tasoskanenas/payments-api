# Sample Payments API 

# Description
This is a payments api, a sample crud service written in spring boot.


# Documentation
You can find apip documentation here:
- *Online Documentation: https://documenter.getpostman.com/view/669883/payments-api/RVu5jUTr*
- *Local Documentation: http://localhost:8080/swagger-ui.html*

# Setup
In order to build and run the service, Java 8 and maven 3.x is needed to be isntalled in your machine.

1. Clone the reposiory and cd into its root directory.
2. Run mvn clean install, so it can build and run all the tests.
3. Run mvn spring-boot:run so it will spin up.
4. Then you can try using the api from a rest client like postman or curl.

# Testing
In order to run all the tests run in the root folder of the service: mvn test.

# Owner
Tasos Tsaousis - tasoskanenas@gmail.com