package com.payments.server.controller;

import com.payments.server.AbstractTestConfiguration;
import com.payments.server.dto.PaymentDto;
import com.payments.server.dto.PaymentListDto;
import com.payments.server.service.PaymentService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

public class PaymentControllerTest extends AbstractTestConfiguration {

    @MockBean
    private PaymentService paymentService;

    @Autowired
    private PaymentsController controller;

    private PaymentDto testPayment;

    @Before
    public void init() {
        testPayment = createTestPayment("4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43");
    }


    @Test
    public void testCreatePayment() {
        try {
            given(paymentService.createPayment(Mockito.any(PaymentDto.class))).willReturn(testPayment);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // when
        ResponseEntity<PaymentDto> response = postRequest(PaymentDto.class, "/api/payments", testPayment);
        PaymentDto dto = response.getBody();

        // then
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(dto);
        assertEquals(testPayment.getId(), dto.getId());
        assertNotNull(dto.getAttributes());

    }


    @Test
    public void testRetrievePayment() {

        given(paymentService.retrievePayment(Mockito.anyString())).willReturn(testPayment);

        // when
        ResponseEntity<PaymentDto> response = getRequest(PaymentDto.class, paymentsUri + testPayment.getId());
        PaymentDto dto = response.getBody();

        // then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(dto);
        assertEquals(testPayment.getId(), dto.getId());
        assertNotNull(dto.getAttributes());
    }

    @Test
    public void testUpdatePayment() {
        try {
            given(paymentService.updatePayment(Mockito.anyString(), Mockito.any(PaymentDto.class))).willReturn(testPayment);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // when
        ResponseEntity<PaymentDto> response = putRequest(PaymentDto.class, paymentsUri + testPayment.getId(), testPayment);
        PaymentDto dto = response.getBody();

        // then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(dto);
        assertEquals(testPayment.getId(), dto.getId());
        assertNotNull(dto.getAttributes());
    }

    @Test
    public void testDeletePayment() {

        given(paymentService.deletePayment(Mockito.anyString())).willReturn(true);
        ResponseEntity<String> response = deleteRequest(String.class, paymentsUri + testPayment.getId());
        // then
        assertEquals(HttpStatus.OK, response.getStatusCode());

        given(paymentService.deletePayment(Mockito.anyString())).willReturn(false);

        // when
        ResponseEntity<PaymentDto> notFoundResponse = getRequest(PaymentDto.class, paymentsUri + testPayment.getId());
        // then
        assertEquals(HttpStatus.NOT_FOUND, notFoundResponse.getStatusCode());
    }

    @Test
    public void testGetAllPayments() {
        List<PaymentDto> payments = new ArrayList<>();
        payments.add(testPayment);
        PaymentListDto dto = new PaymentListDto();
        dto.setData(payments);


        given(paymentService.getAllPayments()).willReturn(dto);
        // when
        ResponseEntity<PaymentListDto> response = getRequest(PaymentListDto.class, "/api/payments");
        PaymentListDto returnedDto = response.getBody();

        // then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(returnedDto);
        assertNotNull(returnedDto.getData());
        assert (returnedDto.getData().size() > 0);

    }

}
