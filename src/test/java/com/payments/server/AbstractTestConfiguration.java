package com.payments.server;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.payments.server.domain.*;
import com.payments.server.dto.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=PaymentsServiceApplicationTest.class, webEnvironment= WebEnvironment.RANDOM_PORT)
public abstract class AbstractTestConfiguration {

	protected String baseUrl;

	protected ObjectMapper mapper;

	protected String paymentsUri = "/api/payments/";

	@Autowired
    private TestRestTemplate restTemplate;

	@Value("${local.server.port}")
	private int port;

	private HttpHeaders headers = new HttpHeaders();


	@Before
	public void setUp() throws Exception {
		this.baseUrl = new URL("http://localhost:" + port + "/").toString();
		mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}

	protected <T> ResponseEntity<T> getRequest(Class<T> clazz, String url, Object... params) {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<T> entity = new HttpEntity<T>(headers);
		return restTemplate.exchange(baseUrl + url, HttpMethod.GET, entity, clazz, params);
	}

	protected <T> ResponseEntity<T> postRequest(Class<T> clazz, String url, Object body) {
        HttpEntity<?> entity = new HttpEntity<>(body);
        return restTemplate.postForEntity(baseUrl + url, entity, clazz);
    }

	protected <T> ResponseEntity<T> putRequest(Class<T> clazz, String url, Object body) {
		HttpEntity<?> entity = new HttpEntity<>(body);
		return restTemplate.exchange(baseUrl + url, HttpMethod.PUT, entity, clazz);
	}

	protected <T> ResponseEntity<T> deleteRequest(Class<T> clazz, String url) {
		HttpEntity<T> entity = new HttpEntity(headers);
		return restTemplate.exchange(baseUrl + url, HttpMethod.DELETE, entity, clazz);
	}

	protected Map getMap(String json, Class keyClass, Class valueClass) throws IOException {
        return mapper.readValue(json, TypeFactory.defaultInstance().constructMapType(HashMap.class, keyClass, valueClass));
    }

    protected PaymentDto createTestPayment(String paymentId){
		AccountDto beneficiaryPartyDto = AccountDto
				.builder()
				.account_number("gb31926819")
				.account_name("T Tsaousis")
				.account_number_code(AccountNumberCodeType.IBAN.name())
				.account_type(0)
				.address("1 The Beneficiary Localtown SE2")
				.bank_id(123123)
				.bank_id_code(BankIdCodeType.GBDSC.name())
				.name("Anastasios T Tsaousis")
				.build();

		AccountDto debtorPartyDto = AccountDto
				.builder()
				.account_number("GR41926819")
				.account_name("T Tsaousis")
				.account_number_code(AccountNumberCodeType.IBAN.name())
				.account_type(0)
				.address("1 The Beneficiary Localtown SE2")
				.bank_id(123123)
				.bank_id_code(BankIdCodeType.GBDSC.name())
				.name("Anastasios T Tsaousis")
				.build();

		LiteAccountDto sponsorPartyDto = LiteAccountDto
				.builder()
				.account_number("BGB10")
				.bank_id(123123)
				.bank_id_code(BankIdCodeType.GBDSC.name())
				.build();


		FxDto fxDto = FxDto
				.builder()
				.contract_reference("FX123")
				.exchange_rate(2.000)
				.original_amount(100.23)
				.original_currency(Currency.USD.name())
				.build();

		ChargeDataDto cd1Dto = ChargeDataDto
				.builder()
				.amount(5.00)
				.currency(Currency.GBP.name())
				.build();

		ChargeDataDto cd2Dto = ChargeDataDto
				.builder()
				.amount(10.00)
				.currency(Currency.USD.name())
				.build();

		ChargeDataDto cd3Dto = ChargeDataDto
				.builder()
				.amount(5.00)
				.currency(Currency.USD.name())
				.build();

		List<ChargeDataDto> senderCharges = new ArrayList<>();
		senderCharges.add(cd1Dto);
		senderCharges.add(cd2Dto);


		ChargesInformationDto ciDto =  ChargesInformationDto
				.builder()
				.bearer_code(BearerCodeType.SHAR.name())
				.receiver_charges_amount(cd3Dto.getAmount())
				.receiver_charges_currency(cd3Dto.getCurrency())
				.sender_charges(senderCharges)
				.build();

		PaymentAttributesDto paDto  = PaymentAttributesDto
				.builder()
				.amount(200.0)
				.beneficiary_party(beneficiaryPartyDto)
				.charges_information(ciDto)
				.currency(Currency.USD.name())
				.debtor_party(debtorPartyDto)
				.fx(fxDto)
				.reference("test reference")
				.numeric_reference(123456)
				.end_to_end_reference("Tasos Test tsaousis")
				.processing_date("2018-04-01")
				.payment_purpose("Testing Payments API")
				.sponsor_party(sponsorPartyDto)
				.payment_scheme(PaymentSchemeType.FPS.name())
				.scheme_payment_type(SchemePaymentType.ImmediatePayment.name())
				.scheme_payment_sub_type(SchemePaymentSubtype.InternetBanking.name())
				.payment_id("123456789012345678")
				.payment_type(PaymentType.Credit.name())
				.build();


		return PaymentDto
				.builder()
				.attributes(paDto)
				.id(paymentId)
				.type(FinancialActionType.Payment.name())
				.organisation_id("743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb")
				.version(0)
				.build();
	}

}
