package com.payments.server.service;

import com.payments.server.AbstractTestConfiguration;
import com.payments.server.dto.PaymentDto;
import com.payments.server.dto.PaymentListDto;
import com.payments.server.repository.PaymentRepository;
import com.payments.server.entity.Payment;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;

public class PaymentServiceTest extends AbstractTestConfiguration {

    @Autowired
    private PaymentService paymentService;

    @MockBean
    private PaymentRepository repository;

    private PaymentDto testPayment1Dto;

    private Payment testPayment1;

    @Before
    public void init(){


        testPayment1Dto = createTestPayment("4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43");

        try {
            testPayment1 = Payment.fromDto(testPayment1Dto);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testCreatePayment(){

        given(repository.save(Mockito.any(Payment.class))).willReturn(testPayment1);
        try {
            PaymentDto createdPaymentDto = paymentService.createPayment(testPayment1Dto);
            assert(createdPaymentDto != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testRetrievePayment(){
        given(repository.findByPaymentId(Mockito.anyString())).willReturn(testPayment1);
        PaymentDto retrievedPayment = paymentService.retrievePayment(testPayment1Dto.getId());
        assert(retrievedPayment != null);
    }

    @Test
    public void testUpdatePayment(){
        given(repository.save(Mockito.any(Payment.class))).willReturn(testPayment1);
        given(repository.findByPaymentId(Mockito.anyString())).willReturn(testPayment1);
        try {
            PaymentDto updatedPaymentDto = paymentService.updatePayment(testPayment1Dto.getId(),testPayment1Dto);
            assert(updatedPaymentDto != null);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testDeletePayment(){
        given(repository.findByPaymentId(Mockito.anyString())).willReturn(null);
        try {
            paymentService.deletePayment(testPayment1Dto.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        PaymentDto retrievedPayment = paymentService.retrievePayment(testPayment1Dto.getId());
        assert(retrievedPayment == null);
    }

    @Test
    public void testGetAllPayments(){
        List<Payment> retrievedPayments = new ArrayList<>();
        retrievedPayments.add(testPayment1);
        given(repository.findAll()).willReturn(retrievedPayments);
        PaymentListDto foundPaymentDtos = paymentService.getAllPayments();
        assert(foundPaymentDtos.getData() != null);
    }
}
