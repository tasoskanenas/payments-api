package com.payments.server.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaymentDto {

    @NotNull
    private String id;
    @NotNull
    private Integer version;
    @NotNull
    private String organisation_id;
    @NotNull
    private PaymentAttributesDto attributes;
    @NotNull
    private String type;
}
