package com.payments.server.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDto {

	@NotNull
	private String account_number;
	@NotNull
	private String account_name;
	@NotNull
	private String account_number_code;
	@NotNull
	private Integer account_type;
	@NotNull
	private String address;
	@NotNull
	private Integer bank_id;
	@NotNull
	private String bank_id_code;
	@NotNull
	private String name;
}
