package com.payments.server.repository;

import com.payments.server.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment,Long> {

	@Transactional(readOnly=true)
	@Query("select distinct p "
			+ "from Payment p "
			+ "inner join fetch p.paymentAttributes pa "
			+ "inner join fetch pa.beneficiaryParty bp "
			+ "inner join fetch pa.sponsorParty sp "
			+ "inner join fetch pa.debtorParty dp "
			+ "inner join fetch pa.chargesInformation ci "
			+ "inner join fetch ci.charges ch "
			+ "inner join fetch pa.fx fx "
	)
	List<Payment> findAll();

	@Transactional(readOnly = true)
	@Query("select distinct p "
			+ "from Payment p "
			+ "inner join fetch p.paymentAttributes pa "
			+ "inner join fetch pa.beneficiaryParty bp "
			+ "inner join fetch pa.sponsorParty sp "
			+ "inner join fetch pa.debtorParty dp "
			+ "inner join fetch pa.beneficiaryParty bp "
			+ "inner join fetch pa.chargesInformation ci "
			+ "inner join fetch ci.charges ch "
			+ "inner join fetch pa.fx fx "
			+ "where p.paymentUuid = :id")
	Payment findByPaymentId(@Param("id") String paymentId);

}
