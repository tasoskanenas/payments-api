package com.payments.server.entity;

import com.payments.server.domain.Currency;
import com.payments.server.dto.FxDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name="foreign_exchange_data")
public class ForeignExchangeData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name="contract_reference")
    @NotNull
    private String contractReference;

    @Column(name="exchange_rate")
    @NotNull
    private Double exchangeRate;

    @Column(name="original_amount")
    @NotNull
    private Double originalAmount;

    @Column(name="original_currency")
    @NotNull
    private Currency originalCurrency;


    public static FxDto toDto(ForeignExchangeData fx){
        return FxDto
                .builder()
                .contract_reference(fx.getContractReference())
                .exchange_rate(fx.getExchangeRate())
                .original_amount(fx.getOriginalAmount())
                .original_currency(fx.getOriginalCurrency().name())
                .build();
    }

    public static ForeignExchangeData fromDto(FxDto dto){
        return ForeignExchangeData
                .builder()
                .contractReference(dto.getContract_reference())
                .exchangeRate(dto.getExchange_rate())
                .originalAmount(dto.getOriginal_amount())
                .originalCurrency(Currency.valueOf(dto.getOriginal_currency()))
                .build();
    }

}
