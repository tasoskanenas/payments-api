package com.payments.server.entity;

import com.payments.server.domain.BearerCodeType;
import com.payments.server.domain.Currency;
import com.payments.server.domain.FinancialActionChargesType;
import com.payments.server.dto.ChargeDataDto;
import com.payments.server.dto.ChargesInformationDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "charges_information")
@Builder
@AllArgsConstructor
public class ChargesInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "bearer_code")
	@NotNull
	private BearerCodeType bearerCode;

	@NotNull
	@OneToMany(mappedBy = "chargesInformation", fetch = FetchType.EAGER, cascade = CascadeType.ALL,orphanRemoval = true)
	private List<ChargeData> charges = new ArrayList<>();

	public void addCharge(ChargeData chargeData) {
		charges.add(chargeData);
		chargeData.setChargesInformation(this);
	}

	public void removeCharge(ChargeData chargeData) {
		charges.remove(chargeData);
		chargeData.setChargesInformation(null);
	}

	public static ChargesInformationDto toDto(ChargesInformation chargesInformation) {

		ChargesInformationDto dto = new ChargesInformationDto();
		dto.setBearer_code(chargesInformation.getBearerCode().name());
		List<ChargeDataDto> senderChargesDtos = new ArrayList<>();

		for(ChargeData cd : chargesInformation.getCharges()){
			if(cd.getType().equals(FinancialActionChargesType.sender)){
				senderChargesDtos.add(ChargeData.toDto(cd));
			} else {
				dto.setReceiver_charges_amount(cd.getAmount());
				dto.setReceiver_charges_currency(cd.getCurrency().name());
			}
		}
		dto.setSender_charges(senderChargesDtos);
		return dto;
	}


	public static ChargesInformation fromDto(ChargesInformationDto dto){
		ChargesInformation ci = ChargesInformation
				.builder()
				.bearerCode(BearerCodeType.valueOf(dto.getBearer_code()))
				.build();

		List<ChargeData> charges = new ArrayList<>();
		for(ChargeDataDto cdd : dto.getSender_charges()){
			ChargeData cd = ChargeData
					.fromDto(ci,cdd);
					cd.setType(FinancialActionChargesType.sender);
			charges.add(cd);
		}

		ChargeData cd = new ChargeData();
		cd.setAmount(dto.getReceiver_charges_amount());
		cd.setCurrency(Currency.valueOf(dto.getReceiver_charges_currency()));
		cd.setType(FinancialActionChargesType.receiver);
		cd.setChargesInformation(ci);
		charges.add(cd);
		ci.setCharges(charges);

		return ci;
	}

}
