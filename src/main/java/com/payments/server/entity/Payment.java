package com.payments.server.entity;

import com.payments.server.domain.FinancialActionType;
import com.payments.server.dto.PaymentDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.ParseException;

@Entity
@Data
@Table(name="Payment")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	private FinancialActionType type;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="payment_uuid", unique = true)
	@NotNull
	private String paymentUuid;

	@Column(name="version")
	@NotNull
	private Integer version;

	@Column(name="organisation_id")
	@NotNull
	private String organisationId;

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name="payment_attributes_id")
	@NotNull
	private PaymentAttributes paymentAttributes;

	public static PaymentDto toDto(Payment p){
		return PaymentDto
				.builder()
				.id(p.getPaymentUuid())
				.type(p.getType().name())
				.organisation_id(p.getOrganisationId())
				.version(p.getVersion())
				.attributes(PaymentAttributes.toDto(p.getPaymentAttributes()))
				.build();
	}

	public static Payment fromDto(PaymentDto dto) throws ParseException {
		return Payment
				.builder()
				.paymentUuid(dto.getId())
				.type(FinancialActionType.valueOf(dto.getType()))
				.organisationId(dto.getOrganisation_id())
				.version(dto.getVersion())
				.paymentAttributes(PaymentAttributes.fromDto(dto.getAttributes()))
				.build();
	}

}
