package com.payments.server.entity;

import com.payments.server.domain.AccountNumberCodeType;
import com.payments.server.domain.BankIdCodeType;
import com.payments.server.dto.AccountDto;
import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Data
@Entity
@DiscriminatorValue("DebtorParty")
@PrimaryKeyJoinColumn(name="id")
public class DebtorParty extends Account {

	public DebtorParty(Long id, String accountNumber, String accountName, AccountNumberCodeType accountNumberCode, Integer accountType,
			String address, Integer bankId, BankIdCodeType bankIdCode, String name) {
		super(id, accountNumber, accountName, accountNumberCode, accountType, address, bankId, bankIdCode, name);
	}

	public DebtorParty() {
		super();
	}

	public static DebtorParty fromDto(AccountDto dto){
		DebtorParty dp = new DebtorParty();
		dp.setAccountNumber(dto.getAccount_number());
		dp.setAccountName(dto.getAccount_name());
		dp.setAccountNumberCode(AccountNumberCodeType.valueOf(dto.getAccount_number_code()));
		dp.setAccountType(dto.getAccount_type());
		dp.setAddress(dto.getAddress());
		dp.setBankId(dto.getBank_id());
		dp.setBankIdCode(BankIdCodeType.valueOf(dto.getBank_id_code()));
		dp.setName(dto.getName());

		return dp;
	}
}
