package com.payments.server.entity;

import com.payments.server.domain.BankIdCodeType;
import com.payments.server.domain.AccountNumberCodeType;
import com.payments.server.dto.AccountDto;
import com.payments.server.dto.LiteAccountDto;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="account")
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name="account_number")
	private String accountNumber;

	@Column(name="account_name")
	private String accountName;

	@Column(name="account_number_code")
	private AccountNumberCodeType accountNumberCode;

	@Column(name="account_type")
	private Integer accountType;

	@Column(name="address")
	private String address;

	@NotNull
	@Column(name="bank_id")
	private Integer bankId;

	@NotNull
	@Column(name="bank_id_code")
	private BankIdCodeType bankIdCode;

	@Column(name="name")
	private String name;


	public static AccountDto toDto(Account account){
		return AccountDto
				.builder()
				.account_number(account.getAccountNumber())
				.account_name(account.getAccountName())
				.account_number_code(account.getAccountNumberCode().name())
				.account_type(account.getAccountType())
				.address(account.getAddress())
				.bank_id(account.getBankId())
				.bank_id_code(account.getBankIdCode().name())
				.name(account.getName())
				.build();
	}

	public static LiteAccountDto toLiteDto(Account account){
		return LiteAccountDto
				.builder()
				.account_number(account.getAccountNumber())
				.bank_id(account.getBankId())
				.bank_id_code(account.getBankIdCode().name())
				.build();
	}

	public static Account fromDto(AccountDto dto){
		return Account
				.builder()
				.accountNumber(dto.getAccount_number())
				.accountName(dto.getAccount_name())
				.accountNumberCode(AccountNumberCodeType.valueOf(dto.getAccount_number_code()))
				.accountType(dto.getAccount_type())
				.address(dto.getAddress())
				.bankId(dto.getBank_id())
				.bankIdCode(BankIdCodeType.valueOf(dto.getBank_id_code()))
				.name(dto.getName())
				.build();
	}

    public static Account fromLiteDto(LiteAccountDto dto){
        return Account
                .builder()
                .accountNumber(dto.getAccount_number())
                .bankId(dto.getBank_id())
                .bankIdCode(BankIdCodeType.valueOf(dto.getBank_id_code()))
                .build();
    }
}
